/*
based from https://github.com/marcuswestin/WebViewJavascriptBridge
*/
;(function() {
console.log("entering WebViewJavascriptBridgeAndroid");
	if (window.WebViewJavascriptBridgeAndroid) { return }
	var androidInterface
	var sendMessageQueue = []
	var receiveMessageQueue = []
	var messageHandlers = {}
	
	var CUSTOM_PROTOCOL_SCHEME = 'wvjbscheme'
	var QUEUE_HAS_MESSAGE = '__WVJB_QUEUE_MESSAGE__'
	
	var responseCallbacks = {}
	var uniqueId = 1
	
	function init(interfaceTag) {
console.log("entering WebViewJavascriptBridgeAndroid.init");
		androidInterface = interfaceTag
	}

	function send(data, responseCallback) {
console.log("entering WebViewJavascriptBridgeAndroid.send");
//		_doSend({ data:data }, responseCallback)
	}
	
	function registerHandler(handlerName, handler) {
console.log("entering WebViewJavascriptBridgeAndroid.registerHandler");
		messageHandlers[handlerName] = handler
	}
	
	function callHandler(handlerName, data, responseCallback) {
console.log("entering WebViewJavascriptBridgeAndroid.callHandler ("+handlerName+")");
		_doSend({ handlerName:handlerName, data:data }, responseCallback)
	}

	function android_callback(callbackId, responseData) {
console.log("entering WebViewJavascriptBridgeAndroid.andriod_callback");
		if (callbackId) {
			var responseCallback = responseCallbacks[callbackId]
			if (!responseCallback) { return; }
			responseCallback(responseData)
			delete responseCallbacks[callbackId]
		}
	}

	function _doSend(message, responseCallback) {
console.log("entering WebViewJavascriptBridgeAndroid._doSend");
		if (responseCallback) {
			var callbackId = 'cb_'+(uniqueId++)+'_'+new Date().getTime()
			responseCallbacks[callbackId] = responseCallback
			message['callbackId'] = callbackId
		}

console.log("_doSend, androidInterface("+androidInterface+") handlerName("+message.handlerName+") callbackId("+callbackId+")");
console.log ('_doSend, message.data('+JSON.stringify(message.data)+')');
		window[androidInterface][message.handlerName](JSON.stringify(message.data), callbackId);

/*
		sendMessageQueue.push(message)
		messagingIframe.src = CUSTOM_PROTOCOL_SCHEME + '://' + QUEUE_HAS_MESSAGE
*/
	}

	function _fetchQueue() {
console.log("entering WebViewJavascriptBridgeAndroid._fetchQueue");
/*
		var messageQueueString = JSON.stringify(sendMessageQueue)
		sendMessageQueue = []
		return messageQueueString
*/
	}

	function _dispatchMessageFromObjC(messageJSON) {
console.log("entering WebViewJavascriptBridgeAndroid._dispatchMessageFromObjC");
/*
		setTimeout(function _timeoutDispatchMessageFromObjC() {
			var message = JSON.parse(messageJSON)
			var messageHandler
			
			if (message.responseId) {
				var responseCallback = responseCallbacks[message.responseId]
				if (!responseCallback) { return; }
				responseCallback(message.responseData)
				delete responseCallbacks[message.responseId]
			} else {
				var responseCallback
				if (message.callbackId) {
					var callbackResponseId = message.callbackId
					responseCallback = function(responseData) {
						_doSend({ responseId:callbackResponseId, responseData:responseData })
					}
				}
				
				var handler = WebViewJavascriptBridge._messageHandler
				if (message.handlerName) {
					handler = messageHandlers[message.handlerName]
				}
				
				try {
					handler(message.data, responseCallback)
				} catch(exception) {
					if (typeof console != 'undefined') {
						console.log("WebViewJavascriptBridge: WARNING: javascript handler threw.", message, exception)
					}
				}
			}
		})
*/
	}
	
	function _handleMessageFromObjC(messageJSON) {
console.log("entering WebViewJavascriptBridgeAndroid._handleMessageFromObjC");
/*
		if (receiveMessageQueue) {
			receiveMessageQueue.push(messageJSON)
		} else {
			_dispatchMessageFromObjC(messageJSON)
		}
*/
	}

	window.WebViewJavascriptBridgeAndroid = {
		init: init,
		registerHandler: registerHandler,
		callHandler: callHandler,
		android_callback: android_callback,
	}

	var doc = document
	var readyEvent = doc.createEvent('Events')
	readyEvent.initEvent('WebViewJavascriptAndroidBridgeReady')
	readyEvent.bridge = WebViewJavascriptBridgeAndroid;
	doc.dispatchEvent(readyEvent)

console.log("leaving WebViewJavascriptBridgeAndroid");

})();
