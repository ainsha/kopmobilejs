// JavaScript Document
(function(){ 

   

	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0;



	var nativeBridge = {
		
		connectWebViewJavascriptBridge : function (callback) {
			
		if (deviceIsAndroid) {
			if (window.WebViewJavascriptBridgeAndroid) {
				callback(WebViewJavascriptBridgeAndroid)
			} else {
				document.addEventListener('WebViewJavascriptAndroidBridgeReady', 			function() {
					callback(WebViewJavascriptBridgeAndroid)
					getuserdata();
					getPlayhead();
				}, false)
			}
		} else {
			if (window.WebViewJavascriptBridge) {
				callback(WebViewJavascriptBridge)
			} else {
				document.addEventListener('WebViewJavascriptBridgeReady', function() {
					callback(WebViewJavascriptBridge)
					getuserdata();
					getPlayhead();
				}, false)
			}
		}
		
		
		
	
	},
	
	
	initNativeBridge : function(){
	
		nativeBridge.connectWebViewJavascriptBridge(function(bridge) {
			if (deviceIsAndroid) {
					bridge.init('ANDROID');
		
					android_callback = function(callbackId, responseData) {
						bridge.android_callback(callbackId, responseData)
					};
			} else {	
					 /* Init your app here */
			
				bridge.init(function(message, responseCallback) {
					//alert('Received message: ' + message)   
					if (responseCallback) {
						responseCallback("Right back atcha")
					}
				});
				
				bridge.registerHandler('metadata', function(data, responseCallback) {
						xmetadata(data);
					});
		
				
			} //End Android Check
			
			
			metadata = function(payload) {
				xmetadata(payload)
			};
			
			
		}); //End connect
	}
	
	};

	
 console.log('KBRIDGE START JAVSCRIPT  INIT NATIVE');
	nativeBridge.initNativeBridge();
	
	
	// Generic metadata detected in video stream callback from ios bridge side
	// mainly to update scoreboard now
	function xmetadata(data) {
		// unpack uuencoded packed data from video stream
		var raw = window.atob(data);
		var rawLength = raw.length;
		var array = new Uint8Array(new ArrayBuffer(rawLength));
		for (i = 0; i < rawLength; i++) {
			array[i] = raw.charCodeAt(i);
		}
	
		// Only use this for debugging
		// for (var i=21; i<29; i++) {
		// 	console.debug("DUMP metadata:" + i + " == " + array[i]);
		// }
		var update_data = {}
	
		// this is a wip TODO: call a function to update these with the appropriate fields populated
		if (rawLength > 18) {
			if (array[18] == 0xff) {
	
				try {
	
					var gametimestr = array[21] + ":" + zeroPad(array[22], 2);
	
					var shotclock = array[23] << 8 | array[24];
					update_data['game_clock'] = gametimestr;
					shotclock /= 1000.0;
					var shotclockp;
					if (shotclock < 10.0) {
						var shotclockp = Math.round(shotclock * 10) / 10;
					} else {
						var shotclockp = Math.round(shotclock);
					}
					// TODO: uncomment to enable shot clock
					update_data['shot_clock'] = shotclockp;
					var period;
					if (array[25] > 0) {
						switch (array[25]) {
						case 1:
							period = "1st";
							break;
						case 2:
		
							period = '2nd';
							break;
						case 3:
							period = '3rd';
							break;
						case 4:
							
							period = '4th';
							break;
						default:
							// Display 1OT, 2OT, 3OT, etc
							period = (array[25] - 4);
							period += 'OT';
							break;
						}
	
					} else {
						period = "PRE";
					}
					update_data['game_period'] = period;
					update_data['team1_score'] = array[26]; // first is home
					update_data['team2_score'] = array[27]; // second is away
					
					 pubsub.publish("scoreupdate",[update_data]);
				} catch (e) {
					console.log("Metadata Error handler "+e.message);
				}
			}
	
			// TODO: only update fields that have changed since last update (do above in byte mode as it's easier)
			/*for (var txtid in update_data) {
				if (update_data.hasOwnProperty(txtid)) {
					KISWE.OSD.updateScoreboardItem(txtid, update_data[txtid]);
				}
			}*/
		}
		
		
	}// End meta data

 function zeroPad(number, digits) {
    	return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
	}

})();
